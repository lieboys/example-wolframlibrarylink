#pragma once
#include "LibraryLinkExamples.h"

namespace LLE {



	/// Function that adds two Reals 
	int AddDoubles(WolframLibraryData libData, mint Argc, MArgument *Args, MArgument Res) {
		
		// get input variables and convert the to Doubles 
		double x = MArgument_getReal(Args[0]);
		double y = MArgument_getReal(Args[1]);

		// add the two numbers 
		double sum = x + y;

		// attach the result to the output variable
		MArgument_setReal(Res, sum);

		// return error code
		return LIBRARY_NO_ERROR;

	}

	/// function that adds two vectors by summing elements at the same position
	int AddVectors(WolframLibraryData libData, mint Argc, MArgument *Args, MArgument Res) {

		// get input variables 
		MTensor A = MArgument_getMTensor(Args[0]);
		MTensor B = MArgument_getMTensor(Args[1]);

		// convert MTensor to std::vector
		std::vector<double> stdVecA = MathematicaIO::MTensorToVector<double>(libData, A);
		std::vector<double> stdVecB = MathematicaIO::MTensorToVector<double>(libData, B);

		// add the two vectors
		std::transform(stdVecA.begin(), stdVecA.end(), stdVecB.begin(), stdVecA.begin(), std::plus<double>());

		// transform back to MTensor
		MTensor mTensor = MathematicaIO::VectorToMTensor<double>(libData, stdVecA);

		// Set Output argument
		MArgument_setMTensor(Res, mTensor);

		// return error code
		return LIBRARY_NO_ERROR;
	}

	/// function that inverts a string
	int StringInvert(WolframLibraryData libData, mint Argc, MArgument * Args, MArgument Res) {
		
		// get the input string
		char* str = MArgument_getUTF8String(Args[0]);
		
		// reverse the string 
		std::reverse(str, str + strlen(str));

		// set the return string no the output var
		MArgument_setUTF8String(Res, str);
		
		// return error code
		return LIBRARY_NO_ERROR;
	}

	/// function to demonstrate how to push stuff back to the Mathematica front-end, note that mathlink is required for this.
	int printSomeInternal(WolframLibraryData libData, mint Argc, MArgument * Args, MArgument Res) {

		// Create a string 
		int value = 1;
		char buffer[50];
		int n = sprintf_s(buffer, sizeof(buffer),"Runtime-value: %d", value);

		// Create MLink
		MLINK link = libData->getWSLINK(libData);

		// Create expression: EvaluatePacket[Print[string]]
		MLPutFunction(link, "EvaluatePacket", 1); // 
		MLPutFunction(link, "Print", 1); 
		MLPutString(link, buffer); 

		// Execute link expression
		libData->processWSLINK(link);

		// Return error code 
		return LIBRARY_NO_ERROR;

	}


}