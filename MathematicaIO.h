#pragma once
#include "WolframLibrary.h"
#include <vector>

namespace MathematicaIO{

	/// Convert MTensor to std::vector
	template<typename T> std::vector<T> MTensorToVector(WolframLibraryData libData, MTensor mTensor) {

		// get the data from the MTensor object
		T* data = libData->MTensor_getRealData(mTensor);

		//get the number of elements
		mint l = libData->MTensor_getFlattenedLength(mTensor);

		// create vector object
		std::vector<T> vec(data, data + l);

		// return the std::vector 
		return vec;

	}

	/// Convert std::vector to a MTensor
	template<typename T> MTensor VectorToMTensor(WolframLibraryData libData, std::vector<T> vec) {

		// init vars
		int i,err;
		mint l;
		MTensor mTensor;
		mreal* data; 

		// get the number of elements
		l = vec.size();

		// initialize a new tensor on mTensor
		err = libData->MTensor_new(MType_Real, 1, &l, &mTensor);
		data = libData->MTensor_getRealData(mTensor);
		
		// Set the data of the tensor
		for (i = 0; i < l; i++) {
			data[i] = vec[i];
		}
		
		// Return the MTensor
		return mTensor;

	}



}