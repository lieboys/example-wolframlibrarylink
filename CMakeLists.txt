# ################################## Project Settings

cmake_minimum_required (VERSION 3.8)
project (Example_LibraryLink)



# ################################## Create DLL

# Export DLL 
add_library(LibraryLinkExample SHARED
	MathematicaIO.h
	MathematicaIO.cpp
	LibraryLinkExamples.h
	LibraryLinkExamples.cpp
)


# ################################## Find mathematica

# Include the CMAKE Mathematica Librarues
set (CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/CMake/Mathematica")
set (Mathematica_USE_LIBCXX_LIBRARIES ON)

# finds newest Mathematica installation and its default components
find_package(Mathematica COMPONENTS WolframLibrary WSTP MathLink)


# Check that Mathematica front-end has been found
if (Mathematica_FOUND)
	message(STATUS "Mathematica Version ${Mathematica_VERSION}")
	message(STATUS "Mathematica Target System IDs ${Mathematica_SYSTEM_IDS}")
	message(STATUS "Mathematica Host System IDs ${Mathematica_HOST_SYSTEM_IDS}")
	message(STATUS "Mathematica Base Directory ${Mathematica_BASE_DIR}")
	message(STATUS "Mathematica User Base Directory ${Mathematica_USERBASE_DIR}")
else()
	message(FATAL_ERROR "Mathematica can not be found")
endif()


# Wolfram Library Link
if (Mathematica_WolframLibrary_FOUND)
	include_directories(${Mathematica_INCLUDE_DIRS})
	message(STATUS "Mathematica Library ${Mathematica_INCLUDE_DIRS}")
else()
	message(FATAL_ERROR "Mathematica LibraryLink not found ")
endif()


#WSTP Link
if(Mathematica_WSTP_FOUND)
	include_directories(${Mathematica_WSTP_INCLUDE_DIR})
	message(STATUS "WSTP Link ${Mathematica_WSTP_INCLUDE_DIR}")
else()
	message(FATAL_ERROR "Mathematica WSTP not found")
endif()

# MathLink
if(Mathematica_MathLink_FOUND)
	include_directories(${Mathematica_MathLink_INCLUDE_DIR})
	target_link_libraries (LibraryLinkExample ${Mathematica_MathLink_LIBRARIES})
	message(STATUS "MathLink ${Mathematica_MathLink_INCLUDE_DIR}")
else()
	message(FATAL_ERROR "Mathematica MathLink not found")
endif()






# ################################## Create DLL

#Copy files the the build
configure_file("LoadLibraryLinkExample.m" "LoadLibraryLinkExample.m" COPYONLY)
