# Example - WolframLibraryLink

This repository shows examples on how to use the Wolfram Library Link (and some MathLink too). Examples that are included at this stage:
 
* CMAKE file (to include Mathematica, MathLink, WSTP, LibraryLink)
* AddDoubles, read arguments and output the result
* AddVectors, read arguments, convert Mathetmatica Types and output the result
* StringInvert, working with strings
* PrintSomeInternal, demonstates how to execute Mathematica code from C++

# How to use

1. Download Repository using Git
2. Install newest version of Mathematica
3. Create project files Using CMAKE
4. Open project and build
5. Run the Mathematica file `LoadLibraryLinkExample.m` (make sure you adapt the path's to your system)