#pragma once 

// include wolfram Library
#include "mathlink.h" // needs to be called before WolframLibrary.h
#include "WolframLibrary.h"


// include Mathematica IO 
#include "MathematicaIO.h"


#include <algorithm>



// create namespace LLE (Library Link Example)
namespace LLE {

	/// function that returns the version of the WolframLibrary header
	EXTERN_C DLLEXPORT mint WolframLibrary_getVersion() {
		return WolframLibraryVersion;
	}

	/// Passing in a data structure that matches the version of the WolframLibrary header used to build the library
	EXTERN_C DLLEXPORT int WolframLibrary_initialize(WolframLibraryData libData) {
		return 0;
	}

	/// When the library is no longer used by the Wolfram Language it will call a function to shut down the library
	EXTERN_C DLLEXPORT void WolframLibrary_uninitialize(WolframLibraryData libData) {
		return;
	}

	/// Example function that adds two doubles
	EXTERN_C DLLEXPORT int AddDoubles(WolframLibraryData libData, mint Argc, MArgument *Args, MArgument Res);

	/// Example function that adds two vectors
	EXTERN_C DLLEXPORT int AddVectors(WolframLibraryData libData, mint Argc, MArgument *Args, MArgument Res);

	/// Example function to convert RGB-image to grayscale
	EXTERN_C DLLEXPORT int StringInvert(WolframLibraryData libData, mint Argc, MArgument * Args, MArgument Res);

	/// Example function to show you can output stuff to mathematica while computing
	EXTERN_C DLLEXPORT int printSomeInternal(WolframLibraryData libData, mint Argc, MArgument * Args, MArgument Res);
	//libData->evaluateExpression(nullptr,"Print[\"Hi\"]",6,0,0)
	
}

